<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Matricula extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'fechaMatriculacion',
        'nota',
    ];

    protected $guarded = ['id','created_at','updated_at'];
    protected $primaryKey = ['id'];

    public function Course()
    {
        return $this->belongsTo('App\Models\Course','course_id','id');
    }

    public function Estudent()
    {
        return $this->belongsTo('App\Models\Estudent','estudent_id','id');
    }
}
