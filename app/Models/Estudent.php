<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Estudent extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'nombre',
        'paterno',
        'materno',
        'email',
        'ci',
    ];

    protected $guarded = ['id','created_at','updated_at'];
    protected $primaryKey = ['id'];

    public function Matricula()
    {
        return $this->hasMany('App\Models\Matricula','estudent_id','id');
    }

}
