<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'nombre',
        'duracion',
        'portada',
        'fechainicio',
    ];

    protected $guarded = ['id','created_at','updated_at'];
    protected $primaryKey = ['id'];

    public function Matricula()
    {
        return $this->hasMany('App\Models\Matricula','course_id','id');
    }
}
